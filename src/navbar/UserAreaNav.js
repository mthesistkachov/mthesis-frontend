import { useContext } from "react"
import UserContext from "../context/UserContext"
import LogInButton from "./LogInButton";
import UserButton from "./UserButton";


export default function UserAreaNav() {
    const { user } = useContext(UserContext);

    if (user) {
        return (<UserButton />)
    } else {
        return (<LogInButton />)
    }

}