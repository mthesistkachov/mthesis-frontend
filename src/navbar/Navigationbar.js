import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container';
import { Link } from "react-router-dom";
import UserAreaNav from './UserAreaNav';


function Navigationbar() {
  return (
    <Navbar bg="dark" variant="dark" style={{ "minHeight": "50px" }}>
      <Container>
        <Link to="/" style={{ textDecoration: 'none' }}>
          <Navbar.Brand>
            mthesis
          </Navbar.Brand>
        </Link>
        <UserAreaNav></UserAreaNav>
      </Container>
    </Navbar >
  );
}

export default Navigationbar;
