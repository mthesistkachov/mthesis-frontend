import { useContext, useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import UserContext from "../context/UserContext";
import AuthService from "../service/AuthService";

const LOGIN_USERNAME = "usernameLoginForm";
const LOGIN_PASSWORD = "passwordLoginForm"

const REGISTER_USERNAME = "usernameRegisterForm"
const REGISTER_PASSWORD = "passwordRegisterForm"
const REGISTER_FIRSTNAME = "firstnameRegisterForm"
const REGISTER_LASTNAME = "lastnameRegisterForm"

let checkUsernameTimeout;

export default function LogInButton() {
    const [usernameAvailable, setUsernameAvailable] = useState(false);
    const [usernameTaken, setUsernameTaken] = useState(false);
    const [registerInvalid, setRegisterInvalid] = useState(false);
    const [loginInValid, setLoginInValid] = useState(false);

    const { setUser } = useContext(UserContext);

    const [show, setShow] = useState(false);
    const handleShow = () => {
        setShow(true);
    }
    const handleClose = () => {
        setLoginInValid(false);
        clearTimeout(checkUsernameTimeout);
        setShow(false);
    }


    const loginHandler = async (evt) => {
        evt.preventDefault()

        const username = evt.target[LOGIN_USERNAME].value;
        const password = evt.target[LOGIN_PASSWORD].value;

        const ok = await AuthService.login(username, password);
        if (ok) {
            const fetchedUser = await AuthService.getUserInformation();
            setUser(fetchedUser);
            handleClose();
        } else {
            setLoginInValid(true);
        }

    }

    const checkUsernameHandler = (evt) => {
        clearTimeout(checkUsernameTimeout);

        if (usernameAvailable) {
            setUsernameAvailable(false);
        }

        if (usernameTaken) {
            setUsernameTaken(false);
        }

        const username = evt.target.value;
        checkUsernameTimeout = setTimeout(async () => {
            const available = await AuthService.isUsernameAvailable(username);
            if (available) {
                setUsernameAvailable(true);
            } else {
                setUsernameTaken(true);
            }
        }, 500);
    }

    const registerUserHandler = async (evt) => {
        evt.preventDefault();

        const username = evt.target[REGISTER_USERNAME].value;
        const password = evt.target[REGISTER_PASSWORD].value;
        const firstname = evt.target[REGISTER_FIRSTNAME].value;
        const lastname = evt.target[REGISTER_LASTNAME].value;

        const ok = await AuthService.register(username, password, firstname, lastname);
        if (ok) {
            const fetchedUser = await AuthService.getUserInformation();
            setUser(fetchedUser);
            handleClose();
        } else {
            setRegisterInvalid(true);
        }
    }

    return (
        <>
            <Button variant="primary" size="sm" onClick={handleShow}>
                Sign In
            </Button>

            <Modal show={show} onHide={handleClose} size="lg" >
                <Modal.Body>
                    <div className="d-flex">
                        <div className="flex-grow-1 me-3" onSubmit={evt => registerUserHandler(evt)}>
                            <Form>
                                <div className="fs-3 mb-2">Register</div>

                                <p>New here? Create an account now!</p>

                                <Form.Group className="mb-3" controlId={REGISTER_USERNAME}>
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control placeholder="Username" onChange={(evt) => { setRegisterInvalid(false); checkUsernameHandler(evt) }} isInvalid={usernameTaken || registerInvalid} isValid={usernameAvailable} />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId={REGISTER_FIRSTNAME}>
                                    <Form.Label>First Name</Form.Label>
                                    <Form.Control placeholder="First Name" isInvalid={registerInvalid} onChange={() => setRegisterInvalid(false)} />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId={REGISTER_LASTNAME}>
                                    <Form.Label>Last name</Form.Label>
                                    <Form.Control placeholder="Last name" isInvalid={registerInvalid} onChange={() => setRegisterInvalid(false)} />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId={REGISTER_PASSWORD}>
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Password" isInvalid={registerInvalid} onChange={() => setRegisterInvalid(false)} />
                                </Form.Group>

                                <div className="d-flex justify-content-end mt-4">
                                    <Button variant="primary" type="submit">
                                        Register
                                    </Button>
                                </div>
                            </Form>
                        </div>
                        <div className="flex-grow-1 ms-3" onSubmit={evt => loginHandler(evt)}>
                            <Form>
                                <div className="fs-3 mb-2">Sign in</div>

                                <p>Already have an account? Use it to sign in!</p>

                                <Form.Group className="mb-3" controlId={LOGIN_USERNAME}>
                                    <Form.Label>Username</Form.Label>
                                    <Form.Control placeholder="Enter your username" required isInvalid={loginInValid} />
                                </Form.Group>

                                <Form.Group className="mb-3" controlId={LOGIN_PASSWORD}>
                                    <Form.Label>Password</Form.Label>
                                    <Form.Control type="password" placeholder="Enter your password" required isInvalid={loginInValid} />
                                </Form.Group>

                                <div className="d-flex justify-content-end mt-4">
                                    <Button variant="primary" type="submit">
                                        Sign In
                                    </Button>
                                </div>
                            </Form>
                        </div>

                    </div>
                </Modal.Body>
            </Modal>

        </>
    );
}