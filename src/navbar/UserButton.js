import "./UserButton.css"

import React, { useContext } from "react";
import { Dropdown } from "react-bootstrap";
import UserContext from "../context/UserContext";
import AuthService from "../service/AuthService";

export default function UserButton() {
    const { user, setUser } = useContext(UserContext);

    if (!user) {
        console.log("User should be logged in but is not")
        return <></>
    }

    const firstname = user.firstname;
    const lastname = user.lastname;
    const fullname = firstname + " " + lastname;
    const avatarText = firstname[0].toUpperCase() + lastname[0].toUpperCase();

    const toggle = React.forwardRef(({ children, onClick }, ref) => (
        <div ref={ref} onClick={onClick} className="user-button">{avatarText}</div>
    ));

    const logoutHandler = async (evt) => {
        evt.preventDefault();
        AuthService.logout()
        setUser(null);
    }

    return (
        <Dropdown>
            <Dropdown.Toggle as={toggle} id="dropdown-user-actions">
                Custom toggle
            </Dropdown.Toggle>

            <Dropdown.Menu align={{ lg: "start" }} variant="dark">
                <Dropdown.ItemText>Signed in:<br />{fullname}</Dropdown.ItemText>
                <Dropdown.Divider />
                <Dropdown.Item eventKey="1" onClick={logoutHandler}>Sign out</Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    )
}