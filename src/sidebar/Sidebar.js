import { Link } from "react-router-dom"
import "./Sidebar.css"

function createLink(name, path) {
    return {
        name: name,
        path: path,
    }
}

function renderLink(link) {
    return (
        <Link key={link.path} to={link.path} className="sidebar-link-container">
            <div className="sidebar-link">
                {link.name}
            </div>
        </Link>
    )
}

const links = [
    createLink("Home", "/"),
    createLink("View Products", "/products"),
]

export default function Sidebar() {
    return (
        <div>
            <p className="sidebar-title">
                Navigation
            </p>
            <div className="sidebar-links-container">
                {links.map(l => renderLink(l))}
            </div>
        </div>
    )
}
