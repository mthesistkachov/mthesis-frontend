import Navbar from './navbar/Navigationbar'
import HomePage from './pages/home/HomePage'
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import ProductsPage from './pages/products/ProductsPage';
import { Container, Row, Col } from 'react-bootstrap';
import Sidebar from './sidebar/Sidebar';
import ProductPage from "./pages/products/ProductPage";
import AuthService from './service/AuthService';
import { useEffect, useState } from 'react';
import UserContext from './context/UserContext';
import ObjectUtil from './util/ObjectUtil';

function App() {
  const [user, setUser] = useState(null);

  useEffect(() => {
    (async () => {
      if (!user) {
        const fetchedUser = await AuthService.getUserInformation();
        if (!ObjectUtil.equals(user, fetchedUser)) {
          setUser(fetchedUser);
        }
      }
    })();
  }, [user]);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <BrowserRouter>
        <Navbar />
        <Container className="pt-3">
          <Row>
            <Col>
              <Sidebar />
            </Col>
            <Col xs={10}>
              <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/products" element={<ProductsPage />} />
                <Route path="/products/:productId" element={<ProductPage />} />
              </Routes>
            </Col>
          </Row>
        </Container>
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;
