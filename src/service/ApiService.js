import AuthService from "./AuthService";

const ApiService = {}

const HOST = (() => {
    // This should be a variable pointing to a prod env
    const host = process.env.REACT_APP_API_DOMAIN_URL_PREFIX;
    if (host) {
        return host;
    }

    // This hostname is used in local development
    return "http://api.mthesis.com"
})()

ApiService.host = () => {
    return HOST;
}

ApiService.request = async (path, method, body) => {
    if (method === "GET") {
        body = null;
    }

    if (body) {
        body = JSON.stringify(body);
    }

    let authToken = AuthService.getToken();

    const url = HOST + path;

    const headers = {
        "Content-Type": "application/json",
    }

    if (authToken) {
        headers["Authorization"] = "Bearer " + authToken;
    }

    const settings = {
        method: method,
        headers: headers,
        redirect: "error",
        body: body,
    }

    return fetch(url, settings);
}

ApiService.get = async (path) => {
    return ApiService.request(path, "GET");
}

ApiService.post = async (path, body) => {
    return ApiService.request(path, "POST", body);
}

ApiService.put = async (path, body) => {
    return ApiService.request(path, "PUT", body);
}

export default ApiService;
