import ApiService from "./ApiService";

const AuthService = {}

const TOKEN_KEY = "token";

AuthService.storage = window.localStorage;

AuthService.getToken = () => {
    let token = AuthService.storage.getItem(TOKEN_KEY);

    if (token) {
        token = JSON.parse(token).access_token
        return token;
    }

    return undefined;
}

AuthService.saveToken = (token) => {
    token = JSON.stringify(token);
    AuthService.storage.setItem(TOKEN_KEY, token);
}

AuthService.getUserInformation = async () => {
    const response = await ApiService.get("/auth/me");
    if (!response.ok) {
        return null;
    }

    const user = await response.json();
    return user;
}


AuthService.login = async (username, password) => {
    const body = {
        username: username,
        password: password,
    }

    const response = await ApiService.post("/auth/login", body);

    if (!response.ok) {
        return false;
    }

    let token = await response.json();
    AuthService.saveToken(token);
    return true;
}

AuthService.logout = () => {
    AuthService.storage.removeItem(TOKEN_KEY);
}

AuthService.register = async (username, password, firstname, lastname) => {
    const body = {
        username,
        password,
        firstname,
        lastname,
    }

    for (const [key, value] of Object.entries(body)) {
        if (!value) {
            return false;
        }
    }

    const response = await ApiService.post("/auth/register", body);

    if (!response.ok) {
        return false;
    }

    let token = await response.json();
    AuthService.saveToken(token);
    return true;
}

AuthService.isUsernameAvailable = async (username) => {
    const body = {
        username: username,
    }

    const response = await ApiService.put("/auth/usernameavailability", body);

    if (!response.ok) {
        return false;
    }

    let responseBody = await response.json()
    return responseBody.available
}

export default AuthService;
