import { useState } from "react";
import { Button, Form, Modal } from "react-bootstrap";
import { useNavigate, useParams } from "react-router-dom";
import ApiService from "../../service/ApiService";

const FORM_TITLE_ID = "formTitle";
const FORM_RATING_ID = "formRating";
const FORM_DESCRIPTION_ID = "formDescription";

export default function CreateReviewButton() {
    const params = useParams();
    const productId = params.productId;
    const navigate = useNavigate();

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [formValuesAreInValid, setFormValuesAreInvalid] = useState(false);

    const createButtonClickedHandler = async (evt) => {
        evt.preventDefault();

        const title = evt.target[FORM_TITLE_ID].value;
        const rating = +evt.target[FORM_RATING_ID].value;
        const description = evt.target[FORM_DESCRIPTION_ID].value;

        let ok = !!title && !!description && !!rating && rating >= 1 && rating <= 5;

        if (!ok) {
            setFormValuesAreInvalid(!ok);
        }

        let body = {
            title: title,
            rating: rating,
            content: description,
        }

        const path = "/products/" + productId + "/reviews"
        const response = await ApiService.post(path, body)

        if (!response.ok) {
            console.log("Response failed")
            console.log(response)
            const responseBody = await response.text()
            console.log(responseBody)
        } else {
            handleClose()
            // Do this properly some time
            const redirectPath = "/products/" + productId
            navigate("/products", { replace: true })
            navigate(redirectPath, { replace: true })
        }

    }


    return (
        <>
            <Button variant="primary" onClick={handleShow}>
                Create New Review
            </Button>

            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Create a Review</Modal.Title>
                </Modal.Header>

                <Form onSubmit={evt => createButtonClickedHandler(evt)}>

                    <Modal.Body>
                        <Form.Group controlId={FORM_TITLE_ID}>
                            <Form.Label>Titel</Form.Label>
                            <Form.Control type="text" isInvalid={formValuesAreInValid} />
                        </Form.Group>
                        <Form.Group controlId={FORM_RATING_ID}>
                            <Form.Label>Rating</Form.Label>
                            <Form.Control
                                type="number"
                                min="1"
                                max="5"
                                onKeyPress={evt => evt.preventDefault()}
                                isInvalid={formValuesAreInValid}
                            />
                        </Form.Group>
                        <Form.Group controlId={FORM_DESCRIPTION_ID}>
                            <Form.Label>Description</Form.Label>
                            <Form.Control type="text" as="textarea" isInvalid={formValuesAreInValid} />
                        </Form.Group>

                    </Modal.Body>

                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>Close</Button>
                        <Button variant="primary" type="submit">Create Review</Button>
                    </Modal.Footer>
                </Form>

            </Modal>


        </>
    )
}