
const STAR = "\u2605";

export default function Review(props) {
    const review = props.review;

    return (
        <div className="pb-3">
            <h4>{review.title}</h4>
            <div>By: <b>{review.reviewer}</b></div>
            <div>Rating: {STAR.repeat(review.rating)}</div>
            <div>{review.content}</div>
        </div>
    )
}
