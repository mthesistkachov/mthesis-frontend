import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import NumberUtil from "../../util/NumberUtil";
import Spinner from "../../components/Spinner"
import ApiService from "../../service/ApiService";


import "./ProductsPage.css"

async function fetchProducts() {
    const response = await ApiService.get("/products")
    const responseBody = await response.json();
    const products = responseBody.data;
    return products
}

function renderProduct(product) {
    const available = product.available
    const availabilityText = available ? "In stock" : "Not available at the moment"
    const availabilityColor = available ? "text-success" : "text-danger"

    let price = <></>
    if (available) {
        price = (
            <div>{NumberUtil.parsePrice(product.price)}</div>
        )
    }

    return (
        <Link to={"/products/" + product.id} className="product-container" key={product.id}>
            <div className="product-image-container">
                <img src="/img/product-placeholder-small.png" alt={product.name} />
            </div>
            <div className="product-description-container">
                <div className="product-name">
                    {product.name}
                </div>
                <div className={availabilityColor}>
                    {availabilityText}
                </div>
                {price}
            </div>
        </Link>
    )
}

function renderProducts(products) {
    products = products.map(product => renderProduct(product))
    return (
        <div className="product-collection">
            {products}
        </div>
    )
}

export default function ProductsPage() {
    const [products, setProducts] = useState([])
    const [isLoading, setLoading] = useState(true)

    console.log("render")

    useEffect(() => {
        (async () => {
            const fetchedProducts = await fetchProducts();
            setProducts(fetchedProducts);
            setLoading(false);
        })();
    }, []);

    return (
        <div>
            Products are supposed to be here.
            {isLoading ? <Spinner /> : renderProducts(products)}
        </div>
    )
}
