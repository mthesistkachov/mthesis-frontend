import { useContext, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import Spinner from "../../components/Spinner"
import UserContext from "../../context/UserContext";
import ApiService from "../../service/ApiService";
import NumberUtil from "../../util/NumberUtil";
import Review from "./Review";
import CreateReviewButton from "./CreateReviewButton";


async function fetchProduct(id) {
    const response = await ApiService.get("/products/" + id)

    if (!response.ok) {
        return null
    }

    const responseBody = await response.json()
    const product = responseBody.data
    return product
}

async function fetchReviews(productId) {
    const response = await ApiService.get("/products/" + productId + "/reviews")

    if (!response.ok) {
        console.log("Failed to fetch reviews")
        return []
    }

    const responseBody = await response.json()
    const reviews = responseBody.data
    return reviews
}

function renderProduct(product, reviews, user) {
    const available = product.available
    const availabilityText = available ? "In stock" : "Not available at the moment"
    const availabilityColor = available ? "text-success" : "text-danger"

    let price = <></>
    if (available) {
        price = (
            <div>{NumberUtil.parsePrice(product.price)}</div>
        )
    }

    let addReviewButton = <></>
    if (user) {
        addReviewButton = (
            <CreateReviewButton />
        )
    }

    return (
        <div>
            <div className="d-flex flex-row">
                <img src="/img/product-placeholder-big.png" alt={product.name} />
                <div className="product-description-container">
                    <h1>{product.name}</h1>

                    <div className={availabilityColor}>
                        {availabilityText}
                    </div>
                    {price}
                    <p>
                        {product.description}
                    </p>
                </div>
            </div>
            <div className="pt-3">
                <h2>Reviews</h2>
                {addReviewButton}
                <div className="pt-3">
                    {reviews.map(review => <Review key={review.id} review={review} />)}
                </div>
            </div>
        </div>
    )
}

function renderError() {
    return (
        <div>
            Error: Not found
        </div>
    )
}


export default function ProductsPage() {
    const [product, setProduct] = useState(null)
    const [reviews, setReviews] = useState([])
    const [isLoading, setLoading] = useState(true)
    const params = useParams();
    const id = params.productId;

    const { user } = useContext(UserContext);

    useEffect(() => {
        (async () => {
            const fetchProductPromise = fetchProduct(id);
            const fetchReviewsPromise = fetchReviews(id);
            const fetchedProduct = await fetchProductPromise;
            const fetchedReviews = await fetchReviewsPromise;
            setProduct(fetchedProduct);
            setReviews(fetchedReviews);
            setLoading(false);
        })();
    }, [id]);

    let content;

    if (isLoading) {
        content = <Spinner />
    } else if (product) {
        content = renderProduct(product, reviews, user)
    } else {
        content = renderError()
    }

    return content
}
