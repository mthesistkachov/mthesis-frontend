function HomePage() {
    return (
        <div>
            <h1>mthesis - Demo frontend</h1>
            <p>
                This is just a simple Single-Page Application (SPA) for demonstration purposes of the backend applications developed for my master thesis.
                The two backends are developed with different tech stacks (Java+Spring Boot and Go+Go kit) but they share a common interface so this frontend should ideally work with both in the same way.
                Feature-wise it tries to mimic a few functionalites one would encounter on the web site of an online shop.
            </p>
            <p>
                No data here is stored permamently but please still refrain from entering any data belonging to a real person.
                I do not moderate user created content so if you encounter anything inappriate please let me know so I can remove it.
            </p>
            <p>
                If you have any inquiries, please contact me at <a href="mailto: alexandertkachov@gmx.de">alexandertkachov@gmx.de</a>.
            </p>
            <p className="text-end">
                - Alexander Tkachov
            </p>

            <h1>Features</h1>
            Several features of the backends are accessible over this web site.

            <h2>Authentication</h2>
            Account registration and login functionality can be accessed over the button in top right.

            <h2>View Products</h2>
            Showcases HTTP calls to the REST API of the backend.
            It fetches details of products and product reviews one could encounter in an online shop.
        </div>
    )
}

export default HomePage;
