const NumberUtil = {}

NumberUtil.parsePrice = (price) => {
    return price.toFixed(2) + " €"
}

export default NumberUtil;
