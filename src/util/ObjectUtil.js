const ObjectUtil = {}

ObjectUtil.equals = (a, b) => {
    if (a === b) {
        return true;
    }

    if ((!a && b) || (a && !b)) {
        return false;
    }

    return Object.entries(a).sort().toString() === Object.entries(b).sort().toString();
}

export default ObjectUtil;
