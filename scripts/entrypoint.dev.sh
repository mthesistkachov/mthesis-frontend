#!/bin/bash

set -e

# Download exact versions of dependencies
npm ci

npm start
