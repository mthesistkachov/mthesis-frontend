FROM node:17.6 AS build

ENV REACT_APP_API_DOMAIN_URL_PREFIX https://api.mthesis.redcloudwave.com

RUN useradd -ms /bin/bash app
USER app
RUN mkdir -p /home/app/mthesis

WORKDIR /home/app/mthesis

# Download dependencies
COPY package.json package-lock.json ./
RUN npm ci

COPY . .

RUN npm run build

FROM nginx:stable-alpine
COPY --from=build /home/app/mthesis/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
